import pygame
import os
import queue
import threading
import logging
import random


def _get_audio_files(directory):
    return [file for file in os.listdir(directory) if file.lower().endswith('.mp3')]


class WavPlayer:
    def __init__(self, dynamic_audio_dir="./google_text_to_speech/audio_files/"):
        pygame.init()
        self.audio_queue = queue.Queue()
        self.playback_thread = None
        self.is_playing = False
        self.dynamic_audio_dir = dynamic_audio_dir

    def _play_async(self):
        try:
            while not self.audio_queue.empty():
                audio_path = self.audio_queue.get()
                try:
                    audio = pygame.mixer.Sound(audio_path)
                    audio.play()
                    while pygame.mixer.get_busy():
                        pygame.time.delay(100)
                finally:
                    # Only delete if it's a dynamically generated file
                    if audio_path.startswith(self.dynamic_audio_dir):
                        try:
                            os.remove(audio_path)
                            logging.info(f"Removed dynamic audio file: {audio_path}")
                        except OSError as e:
                            logging.error(f"Error removing audio file {audio_path}: {e}")
        except pygame.error as e:
            logging.error(f"Error playing audio: {e}")
        finally:
            self.is_playing = False

    def play(self, audio_path):
        self.audio_queue.put(audio_path)
        if not self.is_playing:
            self.is_playing = True
            self.playback_thread = threading.Thread(target=self._play_async)
            self.playback_thread.start()

    def stop(self):
        while not self.audio_queue.empty():
            self.audio_queue.get()
        pygame.mixer.stop()

    def quit(self):
        self.stop()
        pygame.quit()

    def is_playback_finished(self):
        return not self.is_playing and self.audio_queue.empty()

    def play_random_from_dir(self, directory):
        logging.info(f"Playing audio from {directory}")
        if not os.path.exists(directory):
            logging.error(f"Directory not found: {directory}")
            return

        try:
            audio_files = _get_audio_files(directory)
            if not audio_files:
                logging.error("No audio files found in the directory.")
                return

            file = random.choice(audio_files)
            self.play(os.path.join(directory, file))
        except Exception as e:
            logging.error(f"Error during playback: {e}")

    def error(self, language="fr", gender="M", fun=False):
        voice_type = "male_voice"
        if gender == "F":
            voice_type = "female_voice"
        if fun:
            voice_type = "fun_voice"
        error_dir = os.path.join("/home/lexit/Perso/virtual_assitant/reader/static_audio",
                                 f"error_{language}", voice_type)
        self.play_random_from_dir(error_dir)

    def initialization(self,  language="fr", gender="M", fun=False):
        voice_type = "male_voice"
        if gender == "F":
            voice_type = "female_voice"
        if fun:
            voice_type = "fun_voice"
        init_dir = os.path.join("/home/lexit/Perso/virtual_assitant/reader/static_audio",
                                f"init_{language}", voice_type)
        self.play_random_from_dir(init_dir)

# Example usage
# if __name__ == "__main__":
#     logging.basicConfig(level=logging.INFO)
#     player = WavPlayer()
#     player.initialization("en", "F")
#     player.error("en", "F")
#     player.quit()
