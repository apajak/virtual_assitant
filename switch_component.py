from PyQt5 import QtWidgets, QtCore


class Switch(QtWidgets.QCheckBox):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFixedSize(40, 20)
        self.setStyleSheet("""
        QCheckBox {
            background-color: #ccc;
            border-radius: 10px;
        }
        QCheckBox::indicator {
            background-color: #fff;
            border: 1px solid #777;
            width: 16px;
            height: 16px;
            border-radius: 8px;
            margin: 2px;
        }
        QCheckBox::indicator:checked {
            background-color: #55aa55;
            border: 1px solid #777;
            width: 16px;
            height: 16px;
            border-radius: 8px;
            margin-left: 22px;
        }
        """)

    def sizeHint(self):
        return QtCore.QSize(80, 40)
