import sys
from PyQt5 import QtWidgets, QtGui
import json
from virtual_assistant import VirtualAssistant
from transcription_dialog import TranscriptionDialog
from settings_dialog import SettingsDialog


def load_settings():
    try:
        with open("settings.json", "r") as file:
            return json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        return {"language": "fr", "gender": "M", "fun": False, "username": "Lexit"}


class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        super().__init__(icon, parent)
        self.setToolTip('Virtual Assistant')

        # Callback functions for user input and AI response
        def on_user_input(input_text):
            print(f"User Input: {input_text}")
            self.transcription_dialog.add_transcription_entry("Userdvz", input_text)

        def on_ai_response(response_text):
            print(f"AI Response: {response_text}")
            self.transcription_dialog.add_transcription_entry("Assistant", response_text)

        # Load settings and create VirtualAssistant instance with callbacks
        settings = load_settings()
        self.assistant = VirtualAssistant(**settings,
                                          on_user_input=on_user_input,
                                          on_ai_response=on_ai_response)

        # Initialize TranscriptionDialog with a reference to the assistant
        self.transcription_dialog = TranscriptionDialog(assistant=self.assistant)
        # Initialize VirtualAssistant with signal connections
        self.assistant = VirtualAssistant(language=settings.get("language", "fr"),
                                          gender=settings.get("gender", "M"),
                                          fun=settings.get("fun", False),
                                          username=settings.get("username", "Lexit"),
                                          on_user_input=self.transcription_dialog.update_transcription_signal.emit,
                                          on_ai_response=self.transcription_dialog.update_transcription_signal.emit)



        menu = QtWidgets.QMenu(parent)

        start_action = menu.addAction("Start Assistant")
        start_action.triggered.connect(self.start_assistant)

        stop_action = menu.addAction("Stop Assistant")
        stop_action.triggered.connect(self.stop_assistant)

        transcription_action = menu.addAction("Show Transcription")
        transcription_action.triggered.connect(self.show_transcription_dialog)

        settings_action = menu.addAction("Settings")
        settings_action.triggered.connect(self.show_settings_dialog)

        exit_action = menu.addAction("Exit")
        exit_action.triggered.connect(self.exit_application)

        self.setContextMenu(menu)
        # Set up the context menu
        self.setupContextMenu()

    def setupContextMenu(self):
        menu = QtWidgets.QMenu()
        menu.addAction("Start Assistant", self.start_assistant)
        menu.addAction("Stop Assistant", self.stop_assistant)
        menu.addAction("Show Transcription", self.show_transcription_dialog)
        menu.addAction("Settings", self.show_settings_dialog)
        menu.addAction("Exit", self.exit_application)
        self.setContextMenu(menu)

    def show_transcription_dialog(self):
        self.transcription_dialog.show()

    def update_transcription(self, question, answer):
        self.transcription_dialog.add_transcription_entry(question, answer)

    def on_settings_changed(self, new_settings):
        # Call the update_settings method of VirtualAssistant
        if self.assistant:
            self.assistant.update_settings(**new_settings)

    def show_settings_dialog(self):
        if not hasattr(self, 'settings_dialog') or self.settings_dialog is None:
            self.settings_dialog = SettingsDialog(current_settings=load_settings())
            self.settings_dialog.settings_changed.connect(self.on_settings_changed)
            self.settings_dialog.finished.connect(self.on_settings_dialog_closed)

        self.settings_dialog.show()

    def on_settings_dialog_closed(self, result):
        self.settings_dialog = None

    def start_assistant(self):
        if not self.assistant.is_running:
            self.assistant.start_listening()

    def stop_assistant(self):
        if self.assistant.is_running:
            self.assistant.stop_ongoing_processes()
            self.assistant.stop_listening()

    def exit_application(self):
        if self.assistant.is_running:
            self.assistant.stop_ongoing_processes()
            self.assistant.stop_listening()
        QtWidgets.qApp.quit()


def main_app():
    app = QtWidgets.QApplication(sys.argv)
    tray_icon = SystemTrayIcon(QtGui.QIcon("./VA_logo/VirtualAssistant_1.png"))

    QtWidgets.QApplication.setQuitOnLastWindowClosed(False)
    tray_icon.show()
    return app.exec_()


if __name__ == '__main__':
    main_app()
