# Python Weather API

This Python script is designed to manage OpenWeather API and provides various functionalities to retrieve weather data. Let's go through the code and understand its structure and purpose.

## Script Information

- **Author:** Pajak Alexandre
- **Created Date:** 01/10/2021
- **Version:** 1.0

## Description

The script defines a class called `Weather` which acts as a wrapper for the OpenWeather API. It provides methods to retrieve current weather, five-day forecast, and geocoding information using the OpenWeather API.

## Usage

To use the `Weather` class, you can create an instance of it and call its methods to fetch the desired weather information. Here are the available methods:

### `get_CurrentWeather(city_name, unit="metric", language="fr")`

This method retrieves the current weather for a specific city.

- `city_name` (required): Name of the city.
- `unit` (optional): Unit of measurement for the weather data. Defaults to metric system.
- `language` (optional): Language for the weather data. Defaults to French.

### `get_FiveDayForecast(cityName)`

This method retrieves the five-day forecast for a specific city.

- `cityName` (required): Name of the city.

### `get_DirectGeocoding(cityName)`

This method retrieves city data based on its name.

- `cityName` (required): Name of the city.

### `get_ReverseGeocoding(lat, lon)`

This method retrieves city data based on latitude and longitude coordinates.

- `lat` (required): Latitude of the location.
- `lon` (required): Longitude of the location.

## Dependencies

The script imports the following modules:

- `src.api.weather.current_weather`: Module for retrieving current weather data.
- `src.api.weather.direct_geocoding`: Module for retrieving geocoding data.
- `src.api.weather.five_day_forecast`: Module for retrieving five-day forecast data.
- `src.api.weather.reverse_geocoding`: Module for retrieving reverse geocoding data.

## Example Usage

If you run the script directly, it demonstrates an example usage by fetching the current weather for the city of Bordeaux.

Please note that the code snippet for demonstration purposes is currently commented out (`# if __name__=="__main__":`). You can uncomment it and execute the script to see the example in action.
