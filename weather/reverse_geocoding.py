#!/usr/bin/env python3 
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for get City data from his Coords. """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from typing import Any
from dataclasses import dataclass
from dotenv import load_dotenv
from .LocalNames import LocalNames
import os
import requests
from typing import Optional

# global variables
load_dotenv()
API_KEY = os.getenv("API_KEY")

@dataclass
class City:
    """Class to manage City data"""
    # Properties
    # --------------------
    name: str
    local_names: LocalNames
    lat: float
    lon: float
    country: str
    state: Optional[str]
    # --------------------
    # Static Methods
    # -----------------
    @staticmethod
    def from_dict(obj: Any) -> "City":
        """Convert dict to City object"""
        _name = str(obj.get("name"))
        _local_names = LocalNames.from_dict(obj.get("local_names"))
        _lat = float(obj.get("lat"))
        _lon = float(obj.get("lon"))
        _country = str(obj.get("country"))
        _state = str(obj.get("state"))
        return City(_name, _local_names, _lat, _lon, _country, _state)


class Reverse_Geocoding:
    """Class to manage Reverse Geocoding data"""
    # API Globals
    # -----------------
    lon = -0.58
    lat = 44.84
    appid = API_KEY
    units = "metric"
    limit = 1
    city: City
    # -----------------
    # Constructor
    # -----------------
    def __init__(self, lat: float = 44.84, lon: float = -0.58,limit: int = 1):
        """Constructor for Reverse_Geocoding class"""
        self.lat = lat
        self.lon = lon
        self.limit = limit
        self.city = self.getGeocoding(lat, lon)
    # Methods
    # -----------------
    def getGeocoding(self, lat: float, lon: float) -> "City":
        """Get City data from his Coords"""
        url = f"http://api.openweathermap.org/geo/1.0/reverse?lat={lat}&lon={lon}&limit={self.limit}&appid={self.appid}"
        response = requests.get(url)
        if response.status_code == 200:
            return City.from_dict(response.json()[0])
        else:
            print("Error: ", response.status_code)
            return None

    def toString(self):
        """Print City data"""
        print(self.city.name)
        print(self.city.lat)
        print(self.city.lon)
        print(self.city.country)
        print(self.city.state)
        print(self.city.local_names.toString())

    def get_City(self):
        """Return City object"""
        return self.city

    def get_Name(self):
        """Return City name"""
        return self.city.name

    def get_Lat(self):
        """Return City Latitude"""
        return self.city.lat

    def get_Lon(self):
        """Return City Longitude"""
        return self.city.lon

    def get_Country(self):
        """Return City Country"""
        return self.city.country

    def get_State(self):
        """Return City State"""
        return self.city.state

    def get_Local_Names(self, lang="fr"):
        """Return City Local Names"""
        return self.city.local_names.get_LocalNames(lang)


# --------------------------------------------------------------
# Example Usage
# --------------------------------------------------------------
# city = Reverse_Geocoding(48.8588897, 2.3200410217200766)
# city.toString()
# contry = city.get_Country()
# print(contry)
# --------------------------------------------------------------
