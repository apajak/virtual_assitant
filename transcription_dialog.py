from PyQt5 import QtWidgets, QtCore
from PyQt5.Qsci import QsciScintilla, QsciLexerPython
import re
import html


def highlight_code(code):
    # Basic syntax highlighting for Python code
    # This is a simplistic approach and might need further refinement for complex code

    # Keywords, operators, and braces
    keywords = ["def", "return", "if", "else", "for", "while", "break", "continue", "import", "from", "as", "try",
                "except", "class", "None", "True", "False"]
    operators = ["=", "==", "!=", "+", "-", "*", "/", "//", "%", "**", ">", "<", ">=", "<="]
    braces = ["{", "}", "(", ")", "[", "]"]

    # Escape HTML special characters
    code = QtCore.Qt.escape(code)

    # Highlight keywords
    for keyword in keywords:
        code = code.replace(keyword, f"<span style='color: blue; font-weight: bold;'>{keyword}</span>")

    # Highlight operators
    for operator in operators:
        code = code.replace(operator, f"<span style='color: red;'>{operator}</span>")

    # Highlight braces
    for brace in braces:
        code = code.replace(brace, f"<span style='color: green;'>{brace}</span>")

    # Style the code block
    styled_code = (f"<pre style='background-color: #f0f0f0; padding: 10px; border-radius: 5px; "
                   f"font-family: monospace;'>{code}</pre>")

    return styled_code


class TranscriptionDialog(QtWidgets.QDialog):
    update_transcription_signal = QtCore.pyqtSignal(str, str)
    def __init__(self, assistant, parent=None):
        super().__init__(parent)
        self.assistant = assistant
        self.setWindowTitle("Transcription")
        self.setGeometry(200, 200, 600, 400)

        self.layout = QtWidgets.QVBoxLayout(self)

        # QsciScintilla for transcription area
        self.transcription_area = QtWidgets.QTextEdit(self)
        self.transcription_area.setReadOnly(True)
        self.layout.addWidget(self.transcription_area)

        # Ajout de la zone de saisie pour la question
        self.question_input = QtWidgets.QLineEdit(self)
        self.layout.addWidget(self.question_input)

        # Ajout du bouton pour envoyer la question
        self.send_button = QtWidgets.QPushButton("Envoyer", self)
        self.send_button.clicked.connect(self.handle_send_button)
        self.layout.addWidget(self.send_button)

        self.update_transcription_signal.connect(self.add_transcription_entry)

    def handle_send_button(self):
        user_input = self.question_input.text().strip()
        if user_input:
            self.assistant.on_user_input(user_input)  # Process input through assistant
            self.question_input.clear()

    def add_transcription_entry(self, speaker, text):
        # Use HTML for formatting code blocks
        html_text = self.format_as_html(speaker, text)
        self.transcription_area.insertHtml(html_text)
        self.transcription_area.insertPlainText("\n\n")

    def format_as_html(self, speaker, text):
        # Escape HTML special characters
        text = html.escape(text)

        # Replace code blocks with HTML formatted code
        code_blocks = re.findall(r'```(.*?)```', text, re.DOTALL)
        for block in code_blocks:
            # Call the static method correctly
            highlighted_block = highlight_code(block)
            text = text.replace(f'```{block}```', highlighted_block)

        return f"<b>{speaker}:</b> {text}"
