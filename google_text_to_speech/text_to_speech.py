from google.cloud import texttospeech
import os
import uuid
import re


class TextToSpeech:
    def __init__(self, language="fr", gender="F", fun=False):
        os.environ[
            "GOOGLE_APPLICATION_CREDENTIALS"] = "/home/lexit/Perso/virtual_assitant/turnkey-env-407108-d0c7189e9769.json"
        self.client = texttospeech.TextToSpeechClient()
        self.language_code = 'en-US' if language == 'en' else 'fr-FR'

        # Mapping for voice selection
        voices = {
            ('en', 'F', False): ("en-US", "en-US-Studio-O"),
            ('en', 'M', False): ("en-US", "en-US-Studio-Q"),
            ('en', None, True): ("en-IN", "en-IN-Neural2-B"),
            ('fr', 'M', False): ("fr-FR", "fr-FR-Studio-D"),
            ('fr', 'F', False): ("fr-FR", "fr-FR-Studio-A"),
            ('fr', None, True): ("fr-CA", "fr-CA-Neural2-B")
        }

        voice_key = (language, gender if not fun else None, fun)
        if voice_key in voices:
            self.language_code, self.voice_name = voices[voice_key]
        else:
            raise ValueError(f"No voice found for combination: {voice_key}")

    def process_text(self, text):
        # Define the replacement text based on the language
        replacement_text = {
            'fr-FR': "Le code est disponible dans la transcription.",
            'fr-CA': "Le code est disponible dans la transcription.",
            'en-IN': "Code snippet is available in transcription.",
            'en-US': "Code snippet is available in transcription."
        }

        # Regular expression to match code blocks
        code_block_pattern = r"(```.*?```|`.*?`)"

        # Replace code blocks with the appropriate language message
        processed_text = re.sub(code_block_pattern, replacement_text[self.language_code], text, flags=re.DOTALL)

        return processed_text

    def generate_speech(self, text, output_path="./google_text_to_speech/audio_files/"):
        try:
            os.makedirs(output_path, exist_ok=True)
            processed_text = self.process_text(text)
            synthesis_input = texttospeech.SynthesisInput(text=processed_text)
            voice = texttospeech.VoiceSelectionParams(
                language_code=self.language_code,
                name=self.voice_name,
            )
            audio_config = texttospeech.AudioConfig(
                audio_encoding=texttospeech.AudioEncoding.MP3
            )
            response = self.client.synthesize_speech(
                input=synthesis_input, voice=voice, audio_config=audio_config
            )
            uuid_str = str(uuid.uuid4())
            output_wave_path = os.path.join(output_path, f"output_{self.language_code}_{uuid_str}.mp3")
            with open(output_wave_path, "wb") as out:
                out.write(response.audio_content)
            print(f"Audio content written to file '{output_wave_path}'")
            return output_wave_path
        except Exception as e:
            print(f"Error generating speech: {e}")
            return None

# Test
# if __name__ == "__main__":
#     tts = TextToSpeech("fr", "M", False)
#     str = """Here is a simple example:
#
# ```python
# print("Hello, World!")
# ```
#
# When you run this program, it will display the text `Hello, World!` on the screen."""
#
#     path = tts.generate_speech(str)
#     print(f"Generated audio file at: {path}")
