from text_to_speech import TextToSpeech as TTS
import os
import uuid

# Initialize TTS for French male voice
tts_fr_m = TTS("fr", "M", False)

french_start_sentence_list = [
    "Salut! Comment puis-je vous aider aujourd'hui?",
    "Bienvenue! Jarvis à votre service. Comment puis-je vous assister?",
    "Ravi de vous revoir! Avez-vous besoin d'aide pour quelque chose?",
    "Bonjour! Quelle tâche puis-je accomplir pour vous aujourd'hui?",
    "C'est un plaisir de vous aider. Comment puis-je vous être utile?"
]

# Path to save the audio files
static_audio_path = "../reader/static_audio"
init_fr_path = os.path.join(static_audio_path, "init_fr", "male_voice")
os.makedirs(init_fr_path, exist_ok=True)

def generate_and_save_audio(tts, sentences, directory):
    for sentence in sentences:
        audio_filename = f"{uuid.uuid4()}.mp3"
        audio_path = os.path.join(directory, audio_filename)
        tts.generate_speech(sentence, audio_path)
        print(f"Generated '{audio_path}'")

# Generate and save French male voice initial sentences
generate_and_save_audio(tts_fr_m, french_start_sentence_list, init_fr_path)
