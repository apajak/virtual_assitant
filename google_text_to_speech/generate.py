from text_to_speech import TextToSpeech as TTS
import os
import uuid

tts_fr_f = TTS("fr", "f", False)
tts_fr_m = TTS("fr", "m", False)
tts_fr_f_fun = TTS("fr", None, True)
tts_en_f = TTS("en", "f", False)
tts_en_m = TTS("en", "m", False)
tts_en_f_fun = TTS("en", None, True)

french_error_sentence_list = [
    "Je n'ai pas compris votre question. Pouvez-vous la reformuler ?",
    "Désolé, je n'ai pas saisi cela. Pourriez-vous répéter?",
    "Il semble que j'ai rencontré une erreur. Veuillez essayer à nouveau.",
    "Je suis confus par ce que vous avez dit. Pouvez-vous clarifier?",
    "Malheureusement, je ne peux pas traiter cette demande maintenant.",
    "Oups, cela ne s'est pas passé comme prévu. Veuillez réessayer."
    ]
french_start_sentence_list = [
    "Salut! Comment puis-je vous aider aujourd'hui?",
    "Bienvenue! Jarvis à votre service. Comment puis-je vous assister?",
    "Ravi de vous revoir! Avez-vous besoin d'aide pour quelque chose?",
    "Bonjour! Quelle tâche puis-je accomplir pour vous aujourd'hui?",
    "C'est un plaisir de vous aider. Comment puis-je vous être utile?"
    ]
english_error_sentence_list = [
    "I did not understand your question. Can you rephrase it?",
    "Sorry, I couldn't catch that. Could you say it again?",
    "It seems I've run into an error. Please try again.",
    "I'm confused by what you said. Can you clarify?",
    "Unfortunately, I can't process that request right now.",
    "Oops, that didn't go as expected. Please try again."
    ]
english_start_sentence_list = [
    "Hello, how are you? I'm Jarvis, your virtual assistant...",
    "What can I do for you?",
    "Hi! How can I assist you today?",
    "Welcome! Jarvis at your service. How can I help you?",
    "Good to see you again! Do you need help with something?",
    "Hello! What task can I complete for you today?",
    "Pleased to assist you. How can I be of service?"
    ]

reader_path = "../reader"

# create static_audio directory
static_audio_path = "../reader/static_audio"
os.makedirs(static_audio_path, exist_ok=True)

# create init_fr directory
init_fr_path = os.path.join(static_audio_path, "init_fr")
os.makedirs(init_fr_path, exist_ok=True)

# create init_en directory
init_en_path = os.path.join(static_audio_path, "init_en")
os.makedirs(init_en_path, exist_ok=True)

# create error_fr directory
error_fr_path = os.path.join(static_audio_path, "error_fr")
os.makedirs(error_fr_path, exist_ok=True)

# create error_en directory
error_en_path = os.path.join(static_audio_path, "error_en")
os.makedirs(error_en_path, exist_ok=True)

tts_fr_list = [tts_fr_f, tts_fr_m, tts_fr_f_fun]
tts_en_list = [tts_en_f, tts_en_m, tts_en_f_fun]

def generate_and_save_audio(tts, sentences, base_directory, voice_type):
    voice_directory = os.path.join(base_directory, voice_type)
    os.makedirs(voice_directory, exist_ok=True)

    for sentence in sentences:
        audio_filename = f"{uuid.uuid4()}.mp3"
        audio_path = os.path.join(voice_directory, audio_filename)
        tts.generate_speech(sentence, audio_path)
        print(f"Generated '{audio_path}'")

# Voice type mappings
voice_types = {
    tts_fr_f: "female_voice",
    tts_fr_m: "male_voice",
    tts_fr_f_fun: "fun_voice",
    tts_en_f: "female_voice",
    tts_en_m: "male_voice",
    tts_en_f_fun: "fun_voice"
}

# Generate and save audio files for each category and voice
for tts in tts_fr_list:
    generate_and_save_audio(tts, french_start_sentence_list, init_fr_path, voice_types[tts])
    generate_and_save_audio(tts, french_error_sentence_list, error_fr_path, voice_types[tts])

for tts in tts_en_list:
    generate_and_save_audio(tts, english_start_sentence_list, init_en_path, voice_types[tts])
    generate_and_save_audio(tts, english_error_sentence_list, error_en_path, voice_types[tts])