from PyQt5 import QtWidgets, QtGui, QtCore
import json
from switch_component import Switch


class SettingsDialog(QtWidgets.QDialog):
    settings_changed = QtCore.pyqtSignal(dict)  # Signal following snake_case convention

    def __init__(self, parent=None, current_settings=None):
        super().__init__(parent)
        self.setWindowTitle("Settings")
        self.setGeometry(100, 100, 800, 600)
        self.setWindowIcon(QtGui.QIcon("VA_logo/VirtualAssistant_2.png"))

        main_layout = QtWidgets.QVBoxLayout(self)

        # Logo
        logo_label = QtWidgets.QLabel(self)
        pixmap = QtGui.QPixmap("VA_logo/VirtualAssistant_2.png")
        logo_label.setPixmap(pixmap.scaled(400, 200, QtCore.Qt.KeepAspectRatio))
        main_layout.addWidget(logo_label, alignment=QtCore.Qt.AlignCenter)

        # Title
        title_label = QtWidgets.QLabel("Virtual Assistant", self)
        title_label.setStyleSheet("font-size: 32px; font-weight: bold;")
        title_label.setAlignment(QtCore.Qt.AlignCenter)
        main_layout.addWidget(title_label)

        # Language settings
        language_layout = QtWidgets.QHBoxLayout()
        self.language_combo_box = QtWidgets.QComboBox(self)
        self.language_combo_box.addItems(["fr", "en"])
        language_layout.addWidget(QtWidgets.QLabel("Language:"))
        language_layout.addWidget(self.language_combo_box)
        main_layout.addLayout(language_layout)

        # Gender settings
        gender_layout = QtWidgets.QHBoxLayout()
        self.gender_combo_box = QtWidgets.QComboBox(self)
        self.gender_combo_box.addItems(["Male", "Female"])
        gender_layout.addWidget(QtWidgets.QLabel("Voice Gender:"))
        gender_layout.addWidget(self.gender_combo_box)
        main_layout.addLayout(gender_layout)

        # Funny accent
        fun_layout = QtWidgets.QHBoxLayout()
        self.fun_switch = Switch(self)
        fun_layout.addWidget(QtWidgets.QLabel("Funny Accent:"))
        fun_layout.addWidget(self.fun_switch)
        main_layout.addLayout(fun_layout)

        # Username
        username_layout = QtWidgets.QHBoxLayout()
        self.username_line_edit = QtWidgets.QLineEdit(self)
        username_layout.addWidget(QtWidgets.QLabel("Username:"))
        username_layout.addWidget(self.username_line_edit)
        main_layout.addLayout(username_layout)

        # Save Button
        save_button = QtWidgets.QPushButton("Save", self)
        save_button.clicked.connect(self.save_settings)
        main_layout.addWidget(save_button)

        # Load current settings if available
        if current_settings:
            self.load_current_settings(current_settings)

    def save_settings(self):
        settings = {
            "language": self.language_combo_box.currentText(),
            "gender": 'M' if self.gender_combo_box.currentText() == 'Male' else 'F',
            "fun": self.fun_switch.isChecked(),
            "username": self.username_line_edit.text()
        }
        with open("settings.json", "w") as file:
            json.dump(settings, file, indent=4)
        self.settings_changed.emit(settings)
        self.accept()  # Close the dialog

    def closeEvent(self, event):
        # Let the base class handle the close event
        super(SettingsDialog, self).closeEvent(event)

    def load_current_settings(self, settings):
        self.language_combo_box.setCurrentText(settings.get("language", "fr"))
        self.gender_combo_box.setCurrentText("Male" if settings.get("gender", "M") == "M" else "Female")
        self.fun_switch.setChecked(settings.get("fun", False))
        self.username_line_edit.setText(settings.get("username", ""))
