import logging
from pico.hotword_detection import HotwordDetector
from vosk_speech_to_text.vosk_live_transcription import SpeechToText
from gpt4.text_processing import TextProcessing
from reader.reader import WavPlayer
from google_text_to_speech.text_to_speech import TextToSpeech as GoogleTextToSpeech
import threading
import time


class VirtualAssistant:
    def __init__(self, language='fr', gender='M', fun=False, username='Lexit', on_user_input=None, on_ai_response=None):
        self.language = language
        self.gender = gender
        self.fun = fun
        self.username = username

        # Control variables
        self.is_running = False
        self.listen_thread = None
        self.is_processing = False
        self.should_listen_continuously = True

        # hotword detection
        self.hotword_detector = HotwordDetector(
            keywords=["Jarvis-Stop", "jarvis"],  # Add other keywords if needed
            sensitivities=[0.5, 0.8]  # Sensitivity for each keyword
        )
        # speech to text
        self.speech_to_text = SpeechToText(language=self.language)
        # text processing
        self.text_processing = TextProcessing(language=self.language, username=self.username)
        # text to speech
        self.TTS = GoogleTextToSpeech(language=self.language,gender=self.gender,fun=self.fun)
        # wav player
        self.wav_player = WavPlayer()

        # Connect signals to provided callbacks
        if on_user_input:
            self.user_input_signal.connect(on_user_input)
        if on_ai_response:
            self.ai_response_signal.connect(on_ai_response)

    def update_settings(self, language=None, gender=None, fun=None, username=None):
        """
        Update the assistant settings and reload the necessary components.
        """
        # is running
        running = self.is_running
        # Stop ongoing processes
        self.stop_ongoing_processes()
        self.stop_listening()
        # Update the properties if new values are provided
        if language is not None:
            self.language = language
        if gender is not None:
            self.gender = gender
        if fun is not None:
            self.fun = fun
        if username is not None:
            self.username = username

        # Reinitialize components that depend on these settings
        self.speech_to_text = SpeechToText(language=self.language)
        self.text_processing = TextProcessing(language=self.language, username=self.username)
        self.TTS = GoogleTextToSpeech(language=self.language, gender=self.gender, fun=self.fun)

        # Restart listening if it was running
        if running:
            self.start_listening()

        logging.info("Updated assistant settings and reloaded components.")

    def start_listening(self):
        if self.is_running:
            return  # Already running

        self.is_running = True
        self.listen_thread = threading.Thread(target=self._listen_loop)
        self.listen_thread.start()

    def _listen_loop(self):
        self.hotword_detector.listen(self._on_hotword_detected)

    def _on_hotword_detected(self, keyword):
        logging.info(f"Hotword '{keyword}' detected!")

        if keyword == "jarvis" and not self.is_processing:
            # Play initialization audio and wait for it to finish
            self.wav_player.initialization(language=self.language, gender=self.gender, fun=self.fun)
            while self.wav_player.is_playback_finished() == False:
                time.sleep(0.1)

            # Start processing after audio has finished playing
            self.should_listen_continuously = True
            self._start_processing()

        elif keyword == "Jarvis-Stop":
            # Stop ongoing processes
            self.stop_ongoing_processes()
            self.should_listen_continuously = False


    def _start_processing(self):
        self.is_processing = True
        self.process_thread = threading.Thread(target=self._process_audio_input)
        self.process_thread.start()

    def _process_audio_input(self):
        print("Processing audio input")
        user_input = self.speech_to_text.listen(timeout=5)
        if user_input.strip() != "":
            # update ui with user input
            self.user_input_signal.emit(user_input)

            self._generate_ai_response(user_input)
        else:
            print("No user input detected or timeout occurred")
            self.should_listen_continuously = False
        self.is_processing = False

    def _generate_ai_response(self, input_text):
        for sentence in self.text_processing.get_chat_response(input_text):
            print(f"Response: {sentence}")
            # Emit signal for AI response
            self.ai_response_signal.emit(sentence)

            audio_path = self.TTS.generate_speech(sentence)
            if audio_path:
                self.wav_player.play(audio_path)

    def _process_text_input(self, text_input):
        print("Processing text input")
        if self.is_processing:
            print("Already processing text input")
            return
        if text_input != "" or text_input is not None:
            self._generate_ai_response(text_input)

    def _start_continuous_listening(self):
        # Wait for playback to finish before listening for immediate response
        while not self.wav_player.is_playback_finished():
            time.sleep(0.5)

        if self.is_running and not self.is_processing:
            print("Listening for immediate response...")
            self._listen_and_process()

    def _listen_and_process(self):
        user_input = self.speech_to_text.listen(timeout=5)
        if user_input.strip():
            print(f"Immediate user input: {user_input}")
            self._generate_ai_response(user_input)
        else:
            print("No immediate response detected or timeout occurred")
            self.should_listen_continuously = False

    def stop_ongoing_processes(self):
        print("Stop processing")
        self.speech_to_text.stop_listening()
        self.wav_player.stop()
        self.is_processing = False
        self.should_listen_continuously = False

    def stop_listening(self):
        if not self.is_running:
            return  # Not running
        self.is_running = False
        self.hotword_detector.stop()
        if self.listen_thread:
            self.listen_thread.join()

    def quit(self):
        self.stop_listening()
        self.speech_to_text.quit()
        self.wav_player.quit()
        self.text_processing.quit()
        self.TTS.quit()

# Example usage
# if __name__ == "__main__":
#     logging.basicConfig(level=logging.INFO)
#     assistant = VirtualAssistant('fr', 'M', False, 'Lexit')
#     assistant.start_listening()
    # while assistant.is_running:
    #     time.sleep(10)
    #     assistant._process_text_input("comment tu vas?")
