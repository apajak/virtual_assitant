import pvporcupine
import os
from pvrecorder import PvRecorder

class HotwordDetector:
    def __init__(self, keywords, sensitivities):
        self.keywords = keywords
        self.porcupine = None
        self.recorder = None
        self.sensitivities = sensitivities
        self.running = False  # Flag to control the listening loop

    def setup(self):
        access_key = "5Uuzow+iU8ajoSX2MdZAndYQ+afhSKhEI1dKeP8ufBvj3CyJ7W82Wg=="
        models_path = os.path.join(os.path.dirname(__file__), 'models')

        # Initialize Porcupine
        self.porcupine = pvporcupine.create(
            access_key=access_key,
            keyword_paths=[
                os.path.join(models_path, "Jarvis-Stop_en_linux_v3_0_0.ppn"),
                os.path.join(models_path, "Jarvis_en_linux_v3_0_0.ppn"),
            ],

            sensitivities=self.sensitivities
        )

        # Initialize recorder
        self.recorder = PvRecorder(device_index=-1, frame_length=self.porcupine.frame_length)

    def listen(self, callback):
        self.running = True
        self.setup()
        try:
            self.recorder.start()
            while self.running:
                keyword_index = self.porcupine.process(self.recorder.read())
                if keyword_index >= 0:
                    callback(self.keywords[keyword_index])
        except KeyboardInterrupt:
            pass
        finally:
            self.delete()


    def stop(self):
        self.running = False  # Set the flag to False to stop the loop
        if self.recorder:
            self.recorder.stop()
    def delete(self):
        if self.porcupine:
            self.porcupine.delete()
        if self.recorder:
            self.recorder.delete()