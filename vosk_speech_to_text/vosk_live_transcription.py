#!/usr/bin/env python3

import argparse
import queue
import sounddevice as sd
from vosk import Model, KaldiRecognizer
import os
import json
import logging
import time


models_path = "/home/lexit/Perso/virtual_assitant/vosk_speech_to_text/models"

class SpeechToText:
    def __init__(self, language):
        self.is_listening = False
        self.language = language
        self.q = queue.Queue()
        self.user_input = None
        self.model = self.load_model()
        logging.basicConfig(level=logging.INFO)

    def load_model(self):
        model_path = {
            "fr": "vosk-model-fr-0.22",
            "en": "vosk-model-en-us-0.42-gigaspeech"
        }.get(self.language)

        if model_path:
            logging.info("Loading Vosk model...")
            return Model(os.path.join(models_path, model_path))
        else:
            logging.error("Language not supported.")
            raise ValueError("Language not supported")

    def callback(self, indata, frames, time, status):
        if status:
            logging.error(status)
        self.q.put(bytes(indata))

    def listen(self, timeout=2.0) -> str:
        """ Listens for user input with a timeout. """
        try:
            logging.info("Listening...")
            device_info = sd.query_devices(sd.default.device[0], "input")
            samplerate = int(device_info["default_samplerate"])

            with sd.RawInputStream(samplerate=samplerate, blocksize=8000, device=sd.default.device[0],
                                   dtype="int16", channels=1, callback=self.callback):
                self.rec = KaldiRecognizer(self.model, samplerate)
                self.is_listening = True
                last_audio_time = time.time()
                self.user_input = ""

                while self.is_listening:
                    data = self.q.get()
                    if self.rec.AcceptWaveform(data):
                        response = json.loads(self.rec.Result())
                        if response["text"]:
                            self.user_input = response["text"]
                            break
                        last_audio_time = time.time()
                    elif time.time() - last_audio_time > timeout:
                        logging.info("No speech detected within timeout, stopping listening.")
                        break

            return self.user_input or ""

        except KeyboardInterrupt:
            logging.info("\nInterrupted by user")
        except Exception as e:
            logging.error(f"Error in listening: {e}")
        finally:
            self.is_listening = False
            return self.user_input or ""

    def stop_listening(self):
        logging.info("Stop listening...")
        self.is_listening = False

# Example of use
# if __name__ == "__main__":
#     try:
#         speech_to_text = SpeechToText("en")
#         user_input = speech_to_text.listen()
#         logging.info(f"User said: {user_input}")
#     except Exception as e:
#         logging.error(f"Error: {e}")
