import requests
import json
import sseclient

class ResponseGenerator:
    def __init__(self, api_key, model="text-davinci-003", max_tokens=100, temperature=0):
        self.api_key = api_key
        self.model = model
        self.max_tokens = max_tokens
        self.temperature = temperature
        self.responses = []

    def generate_responses(self, prompt):
        req_url = 'https://api.openai.com/v1/completions'
        req_headers = {
            'Accept': 'text/event-stream',
            'Authorization': 'Bearer ' + self.api_key
        }
        req_body = {
            "model": self.model,
            "prompt": prompt,
            "max_tokens": self.max_tokens,
            "temperature": self.temperature,
            "stream": True,
        }

        request = requests.post(req_url, stream=True, headers=req_headers, json=req_body)
        client = sseclient.SSEClient(request)
        sentence = ''
        for event in client.events():
            if event.data != '[DONE]':
                response_text = json.loads(event.data)['choices'][0]['text']
                sentence += response_text
                if response_text.endswith('.'):
                    self.responses.append(sentence)
                    sentence = ''
                    yield response_text

if __name__ == '__main__':
    API_KEY = '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'
    prompt = "What is Python?"

    generator = ResponseGenerator(API_KEY)
    for i, response in enumerate(generator.generate_responses(prompt)):
        print(f"Response {i+1}: {response}")
