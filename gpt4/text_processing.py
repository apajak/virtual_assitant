import datetime
import os
import logging
from openai import OpenAI
import re


class TextProcessing:
    def __init__(self, api_key="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", language="en", username=""):
        self.api_key = api_key
        self.language = language
        os.environ["OPENAI_API_KEY"] = api_key
        self.client = OpenAI()
        self.conversation = []
        self.username = username
        self.initialize_conversation()

    def initialize_conversation(self):
        self.conversation.append({'role': 'system', 'content': self.get_initial_prompt()})

    def get_initial_prompt(self):
        current_date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        greeting = self.get_time_based_greeting()
        return (
            f"{greeting}! You are Jarvis, {self.username} virtual assistant, here to help with {self.username} daily tasks and answer questions.\n"
            f"Today's date is {current_date}.\n"
            f"You answer on anything, and you provide quick and accurate responses in {self.language} language.\n"
            f"Let's make {self.username} day productive !"
        )

    def get_time_based_greeting(self):
        current_hour = datetime.datetime.now().hour
        if 5 <= current_hour < 12:
            return "Good morning"
        elif 12 <= current_hour < 18:
            return "Good afternoon"
        else:
            return "Good evening"

    def get_chat_response(self, full_prompt):
        try:
            response_stream = self.client.chat.completions.create(
                model="gpt-4-1106-preview",
                messages=[{"role": "user", "content": full_prompt}],
                temperature=1,
                max_tokens=256,
                stream=True,
            )

            current_text = ""
            for response in response_stream:
                content = response.choices[0].delta.content
                if content is not None:
                    current_text += content

                    # Use regular expression to split sentences
                    sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', current_text)
                    for sentence in sentences[:-1]:  # Yield all but the last (potentially incomplete) sentence
                        yield sentence.strip()

                    current_text = sentences[-1]  # Keep the last (potentially incomplete) sentence

                if response.choices[0].finish_reason in ['length', 'stop']:
                    yield current_text.strip()
                    break

            self.conversation.append({'role': 'assistant', 'content': current_text.strip()})

        except Exception as e:
            logging.error(f"Error in chat response: {e}")
            return


# Example usage
# if __name__ == '__main__':
#     text_processor = TextProcessing()
#     prompt = "how to print hello world in python?"
#     for sentence in text_processor.get_chat_response(prompt):
#         print(f"Response: {sentence}")
